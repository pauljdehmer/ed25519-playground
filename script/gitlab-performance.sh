#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Runs GitLab Browser Performance testing on the application. Be sure to
run the serve.sh script with the -b option before running this script.

  -h, --help                 Display this help and exit.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=h
LONGOPTS=help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$(docker ps -a | grep ed25519-playground)" ]; then
  echo "Starting..."
else
  echo "Project not running. Have you started the server?"
  exit 1
fi

if [ -d sitespeed-results ]; then
  docker run \
    -it \
    --rm \
    -v $projectdir:/project \
    -w /project \
    alpine \
    /bin/sh -c "rm -r sitespeed-results"
fi

mkdir sitespeed-results

docker run \
  -it \
  --rm \
  --net=host \
  --shm-size=1g \
  -v $projectdir:/sitespeed.io \
  sitespeedio/sitespeed.io \
    --outputFolder sitespeed-results \
    http://localhost:3000