#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Starts the project Docker container and runs an interactive terminal in
the container using /bin/bash.

  -h, --help                 Display this help and exit.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=h
LONGOPTS=help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$(docker ps -a | grep ed25519-playground)" ]; then
  docker exec -it ed25519-playground /bin/bash
else
  docker run \
    -it \
    --rm \
    --name ed25519-playground \
    --net=host \
    -v $projectdir/application:/application \
    -w /application \
    node \
    /bin/bash -c "npm ci"

  docker run \
    -it \
    --rm \
    --name ed25519-playground \
    --net=host \
    -v $projectdir/application:/application \
    -e EXTEND_ESLINT=true \
    -w /application \
    node \
    /bin/bash
fi
