#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Runs GitLab SAST on the application.

  -h, --help                 Display this help and exit.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=h
LONGOPTS=help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ -d application/node_modules ]; then
  docker run \
    -it \
    --rm \
    -v $projectdir:/project \
    -w /project \
    alpine \
    /bin/sh -c "rm -r application/node_modules"
fi

if [ -d application/build ]; then
  docker run \
    -it \
    --rm \
    -v $projectdir:/project \
    -w /project \
    alpine \
    /bin/sh -c "rm -r application/build"
fi

docker run \
  -it \
  --rm \
  -e SAST_EXCLUDED_PATHS=application/node_modules \
  -v $projectdir:/code \
  -v /var/run/docker.sock:/var/run/docker.sock \
  registry.gitlab.com/gitlab-org/security-products/sast /app/bin/run /code
