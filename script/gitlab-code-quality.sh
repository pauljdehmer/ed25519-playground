#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Runs GitLab Code Quality on the application.

  -h, --help                 Display this help and exit.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=h
LONGOPTS=help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

docker run \
  -it \
  --rm \
  -v $projectdir:/code \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -e SOURCE_CODE=$projectdir \
  registry.gitlab.com/gitlab-org/ci-cd/codequality /code

docker run \
  -it \
  --rm \
  -v $projectdir:/project \
  -w /project \
  alpine \
  /bin/sh -c \
    "rm .csslintrc \
    && rm .eslintignore \
    && rm .eslintrc.yml \
    && rm .rubocop.yml \
    && rm coffeelint.json"

docker ps -a | grep "codeclimate" | awk '{print $1}' | xargs docker rm
