#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Starts the project Docker container and runs eslint on the src folder.

  -h, --help                 Display this help and exit.
  -f, --fix                  If provided, attempts to fix prettier
                             errors automatically.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=fh
LONGOPTS=fix,help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
f=n h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      -f|--fix)
        f=y
        shift
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$f" = "y" ]; then
  command="npm run lint:fix"
else
  command="npm run lint"
fi

if [ "$(docker ps -a | grep ed25519-playground)" ]; then
  docker exec -it ed25519-playground /bin/bash -c "$command"
else
  docker run \
    -it \
    --rm \
    --name ed25519-playground \
    -v $projectdir/application:/application \
    -w /application \
    node \
    /bin/bash -c \
      "npm ci \
      && $command"
fi
