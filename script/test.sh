#!/bin/bash

usage() {
  cat <<EOF
Usage: $0 [options]

Starts the project Docker container and runs the test suite.

  -h, --help                 Display this help and exit.
  -c, --coverage             If included, also print a coverage report.
EOF
}
getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi
OPTIONS=ch
LONGOPTS=coverage,help
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    exit 2
fi
eval set -- "$PARSED"
c=n h=n
while true; do
  case "$1" in
      -h|--help)
        usage
        exit 1
        ;;
      -c|--coverage)
        c=y
        shift
        ;;
      --)
        shift
        break
        ;;
      *)
        echo "Programming error"
        exit 3
        ;;
  esac
done

projectdir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"/../
cd $projectdir

if [ "$c" = "y" ]; then
  command="npm run test -- --coverage"
else
  command="npm run test"
fi

if [ "$(docker ps -a | grep ed25519-playground)" ]; then
  docker exec -it ed25519-playground /bin/bash -c "$command"
else
  docker run \
    -it \
    --rm \
    --name ed25519-playground \
    -v $projectdir/application:/application \
    -w /application \
    node \
    /bin/bash -c \
      "npm ci \
      && $command"
fi

if [ "$c" = "y" ]; then
  docker run \
    -it \
    --rm \
    -v $projectdir/application:/application \
    -w /application \
    alpine \
    /bin/sh -c "rm -r coverage"
fi