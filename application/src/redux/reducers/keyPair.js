import { UPDATE_KEY_PAIR } from "../actionTypes";
import { sign } from "tweetnacl";

const keyPair = sign.keyPair();

const initialState = {
  secretKey: keyPair.secretKey,
  publicKey: keyPair.publicKey,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case UPDATE_KEY_PAIR:
      return {
        ...state,
        secretKey: action.newKeyPair.secretKey,
        publicKey: action.newKeyPair.publicKey,
      };
    default:
      return state;
  }
}
