import { UPDATE_MESSAGE } from "../actionTypes";

const initialState = {
  message: "Test Message",
  signature: "",
};

export default function (state = initialState, action) {
  switch (action.type) {
    case UPDATE_MESSAGE:
      return {
        ...state,
        message: action.message,
        signature: action.signature,
      };
    default:
      return state;
  }
}
