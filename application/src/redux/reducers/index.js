import { combineReducers } from "redux";
import keyPair from "./keyPair";
import message from "./message";

export default combineReducers({
  keyPair,
  message,
});
