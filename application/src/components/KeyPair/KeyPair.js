import Button from "@material-ui/core/Button";
import React from "react";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import { UPDATE_KEY_PAIR } from "../../redux/actionTypes";
import { encodeBase64 } from "tweetnacl-util";
import { makeStyles } from "@material-ui/core/styles";
import { sign } from "tweetnacl";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  keyPair: {
    margin: "20px 0",
  },
  textFields: {
    margin: "10px 0",
  },
}));

function KeyPair() {
  const classes = useStyles();
  const keyPairState = useSelector((state) => state.keyPair);
  const dispatch = useDispatch();

  const generateNewKeyPair = () => {
    const newKeyPair = sign.keyPair();
    dispatch({ type: UPDATE_KEY_PAIR, newKeyPair });
  };

  return (
    <Typography
      variant="body1"
      component="div"
      gutterBottom
      className={classes.keyPair}
    >
      <Typography variant="h5">Key Pair</Typography>
      <TextField
        className={classes.textFields}
        id="secret-key"
        label="Secret Key"
        fullWidth
        multiline
        value={encodeBase64(keyPairState.secretKey)}
        variant="outlined"
        InputProps={{
          readOnly: true,
        }}
      />
      <TextField
        className={classes.textFields}
        id="public-key"
        label="Public Key"
        fullWidth
        multiline
        value={encodeBase64(keyPairState.publicKey)}
        variant="outlined"
        InputProps={{
          readOnly: true,
        }}
      />
      <Button
        size="small"
        onClick={generateNewKeyPair}
        variant="contained"
        color="primary"
      >
        Generate New Key Pair
      </Button>
    </Typography>
  );
}

export default KeyPair;
