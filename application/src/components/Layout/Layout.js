import AppBar from "@material-ui/core/AppBar";
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";
import KeyPair from "../KeyPair/KeyPair";
import LockIcon from "@material-ui/icons/Lock";
import MessageSigner from "../MessageSigner/MessageSigner";
import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Verifier from "../Verifier/Verifier";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  appBarLogo: {
    margin: "0 10px 0 0",
  },
}));

function Layout() {
  const classes = useStyles();

  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <LockIcon className={classes.appBarLogo} />
          <Typography variant="h6" color="inherit">
            ed25519 Playground
          </Typography>
        </Toolbar>
      </AppBar>
      <Container maxWidth="lg">
        <Typography variant="body1" component="div" gutterBottom>
          <p>
            A playground for{" "}
            <a
              href="https://en.wikipedia.org/wiki/EdDSA#Ed25519"
              rel="noopener noreferrer"
              target="_blank"
            >
              ed25519 based DSA
            </a>
            . Uses the{" "}
            <a
              href="https://github.com/dchest/tweetnacl-js"
              target="_blank"
              rel="noopener noreferrer"
            >
              tweetnacl
            </a>{" "}
            library.
          </p>
          <p>This playground allows you to:</p>
          <ul>
            <li>Generate Key Pairs</li>
            <li>Sign Messages</li>
            <li>Verify Message Signatures</li>
          </ul>
        </Typography>
        <Divider />
        <KeyPair />
        <Divider />
        <MessageSigner />
        <Divider />
        <Verifier />
      </Container>
    </div>
  );
}

export default Layout;
