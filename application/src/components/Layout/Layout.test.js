import App from "../App/App";
import React from "react";
import { render } from "@testing-library/react";

test("renders test", () => {
  const { getByText } = render(<App />);
  const element = getByText(/ed25519 Playground/i);
  expect(element).toBeInTheDocument();
});
