import { createMuiTheme } from "@material-ui/core/styles";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#323232",
    },
    secondary: {
      main: "#018786",
    },
    background: {
      default: "#fff",
    },
  },
  typography: {
    fontSize: 12,
  },
});

export default theme;
