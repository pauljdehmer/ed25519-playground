import React from "react";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import { UPDATE_MESSAGE } from "../../redux/actionTypes";
import { decodeUTF8 } from "tweetnacl-util";
import { encodeBase64 } from "tweetnacl-util";
import { makeStyles } from "@material-ui/core/styles";
import { sign } from "tweetnacl";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => ({
  messageSigner: {
    margin: "20px 0",
  },
  textFields: {
    margin: "10px 0",
  },
}));

function MessageSigner() {
  const classes = useStyles();
  const keyPairState = useSelector((state) => state.keyPair);
  const messageState = useSelector((state) => state.message);
  const dispatch = useDispatch();

  useEffect(() => {
    let { message, signature } = signMessage(
      messageState.message,
      keyPairState.secretKey
    );
    if (messageState.signature === "") {
      dispatch({ type: UPDATE_MESSAGE, message, signature });
    } else {
      if (encodeBase64(signature) !== encodeBase64(messageState.signature)) {
        dispatch({ type: UPDATE_MESSAGE, message, signature });
      }
    }
  });

  const messageChanged = (event) => {
    let { message, signature } = signMessage(
      event.target.value,
      keyPairState.secretKey
    );
    dispatch({ type: UPDATE_MESSAGE, message, signature });
  };

  const signMessage = (message, secretKey) => {
    return {
      message: message,
      signature: sign.detached(decodeUTF8(message), secretKey),
    };
  };

  return (
    <div className={classes.messageSigner}>
      <Typography variant="h5">Message Signer</Typography>
      <TextField
        className={classes.textFields}
        id="message"
        label="Message"
        fullWidth
        multiline
        value={messageState.message}
        onChange={messageChanged}
        variant="outlined"
      />
      <TextField
        className={classes.textFields}
        id="signature"
        label="Signature"
        fullWidth
        multiline
        variant="outlined"
        value={encodeBase64(messageState.signature)}
        InputProps={{
          readOnly: true,
        }}
      />
    </div>
  );
}

export default MessageSigner;
