import Button from "@material-ui/core/Button";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import HighlightOffIcon from "@material-ui/icons/HighlightOff";
import InputAdornment from "@material-ui/core/InputAdornment";
import React from "react";
import TextField from "@material-ui/core/TextField";
import { Typography } from "@material-ui/core";
import ValidationTextField from "@material-ui/core/TextField";
import { decodeBase64 } from "tweetnacl-util";
import { decodeUTF8 } from "tweetnacl-util";
import { encodeBase64 } from "tweetnacl-util";
import { makeStyles } from "@material-ui/core/styles";
import { sign } from "tweetnacl";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  verifier: {
    margin: "20px 0",
  },
  textFields: {
    margin: "10px 0",
  },
  validationTextField: {
    margin: "10px 0",
    "& textarea:valid~fieldset": {
      border: "2px solid rgb(81, 196, 81)",
    },
  },
}));

function Verifier() {
  const classes = useStyles();
  const [message, setMessage] = useState("");
  const [publicKey, setPublicKey] = useState("");
  const [signature, setSignature] = useState("");
  const [valid, setValid] = useState(false);
  const keyPairState = useSelector((state) => state.keyPair);
  const messageState = useSelector((state) => state.message);

  useEffect(() => {
    validateMessage();
  });

  const validateMessage = () => {
    let valid;
    try {
      valid = sign.detached.verify(
        decodeUTF8(message),
        decodeBase64(signature),
        decodeBase64(publicKey)
      );
    } catch (error) {
      valid = false;
    }
    setValid(valid);
  };

  const updateMessage = (event) => {
    setMessage(event.target.value);
  };

  const updatePublicKey = (event) => {
    setPublicKey(event.target.value);
  };

  const updateSignature = (event) => {
    setSignature(event.target.value);
  };

  const copyValues = (event) => {
    setMessage(messageState.message);
    setPublicKey(encodeBase64(keyPairState.publicKey));
    setSignature(encodeBase64(messageState.signature));
  };

  return (
    <div className={classes.verifier}>
      <Typography variant="h5">Signature Verifier</Typography>
      <TextField
        className={classes.textFields}
        id="message"
        label="Message"
        fullWidth
        multiline
        value={message}
        onChange={updateMessage}
        variant="outlined"
      />
      <TextField
        className={classes.textFields}
        id="public-key"
        label="Public Key"
        fullWidth
        multiline
        value={publicKey}
        onChange={updatePublicKey}
        variant="outlined"
      />
      <ValidationTextField
        className={classes.validationTextField}
        id="signature"
        label="Signature"
        fullWidth
        multiline
        value={signature}
        onChange={updateSignature}
        variant="outlined"
        error={!valid}
        helperText={valid ? false : "Invalid Signature"}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              {valid ? (
                <CheckCircleOutlineIcon style={{ color: "#00CC00" }} />
              ) : (
                <HighlightOffIcon color="error" />
              )}
            </InputAdornment>
          ),
        }}
      />
      <Button
        size="small"
        onClick={copyValues}
        variant="contained"
        color="primary"
      >
        Copy Values From Key Pair and Message Signer
      </Button>
    </div>
  );
}

export default Verifier;
