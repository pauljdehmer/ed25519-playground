[![pipeline status](https://gitlab.com/pauljdehmer/ed25519-playground/badges/master/pipeline.svg)](https://gitlab.com/pauljdehmer/ed25519-playground/-/commits/master)
[![coverage report](https://gitlab.com/pauljdehmer/ed25519-playground/badges/master/coverage.svg)](https://gitlab.com/pauljdehmer/ed25519-playground/-/commits/master)

# ed25519 Playground

_A playground for [ed25519 based DSA](https://en.wikipedia.org/wiki/EdDSA#Ed25519).
Uses the [tweetnacl](https://github.com/dchest/tweetnacl-js) library._

## Dependencies

This project requires [Docker](https://www.docker.com/) to be installed.

## Usage

There are scripts included in the ```script``` directory of this project.

* ```bash.sh```
* ```build.sh```
* ```lint.sh```
* ```serve.sh```
* ```test.sh```

These scripts start or enter a docker container (if already running) with
the ```application``` folder mounted as a volume and execute commands on the
application. Run any of these with the ```--help``` option for more information
about them.

The ```gitlab-*``` scripts run tools and scanners provided by Gitlab on the
project. These are also run in the pipeline. Run any of these with the ```--help```
option for more information about them.

## Application

The ed25519 Playground is a tool that allows you to:

1. Generate Key Pairs
1. Sign Messages
1. Verify Message Signatures

This project was bootstrapped from [Create React App](https://create-react-app.dev/).
Their original ```README.md``` file is unmodified in the ```application```
folder, but be aware this project uses ```npm``` as the package manager instead
of ```yarn```.

## Demo

<https://pauljdehmer.gitlab.io/ed25519-playground/>